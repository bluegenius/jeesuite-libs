package com.jeesuite.mybatis.datasource;

public enum DatabaseType {
    mysql,oracle,postgresql,sqlserver,h2
}
